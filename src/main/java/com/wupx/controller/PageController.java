package com.wupx.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PageController {
    /**
     * 静态页面跳转
     *
     * @author menghe
     * @param page
     * @return
     *
     */
    @GetMapping("{page}")
    public String page(@PathVariable String page) {
        return page;
    }

    /**
     * 暂缓因找不到favicon.ico 而报错的问题
     * https://blog.csdn.net/weixin_45684562/article/details/115894524
     */
    @GetMapping("favicon.ico")
    @ResponseBody
    void returnNoFavicon() {
    }
}
